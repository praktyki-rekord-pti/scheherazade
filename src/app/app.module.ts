import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { BuuuComponent } from './buuu/buuu.component';
import { TodolistComponent } from './todolist/todolist.component';
import { MaketaskComponent } from './maketask/maketask.component';
import { MakecategoryComponent } from './makecategory/makecategory.component';
import { EdituserComponent } from './edituser/edituser.component';
import { UserdeleteComponent } from './userdelete/userdelete.component';
import { UpdateCategoriesComponent } from './update-categories/update-categories.component';
import { ArchiveComponent } from './archive/archive.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    BuuuComponent,
    TodolistComponent,
    MaketaskComponent,
    MakecategoryComponent,
    EdituserComponent,
    UserdeleteComponent,
    UpdateCategoriesComponent,
    ArchiveComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [MenuComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
