import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {GlobalsService} from "../globals.service";

@Component({
  selector: 'app-userdelete',
  templateUrl: './userdelete.component.html',
  styleUrls: ['./userdelete.component.less']
})
export class UserdeleteComponent implements OnInit {

  constructor(private http: HttpClient, public g: GlobalsService) { }
  confirmation = '';
  ngOnInit() {
  }
  logout () {
    this.g.currName = '';
    this.g.currMail = '';
    this.g.currPass = '';
    this.g.currUserid = -1;
    this.g.isLoggedIn = false;
  }
  deleteUser() {
    if (this.confirmation == 'YES') {
      let url = 'http://alastor.overmc.net:34000/api/deleteuser';
      let data = {"Email": this.g.currMail, "Password": this.g.currPass};
      console.log(url, data);
      this.http.post(url, data).subscribe(result => {
        if (result){
          console.log('User hath been nuked');
          this.logout();
        }
      });
    }
  }

}
