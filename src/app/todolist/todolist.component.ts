import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../_model/user";
import {Todosy} from "../_model/todosy";



class sortation{
  IdSorting: number;
  nameSorting: number;
}
@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.less']
})
export class TodolistComponent implements OnInit {

  vacking = new Date('1995-12-17T03:24:00');
  tasks = [new Todosy(0,0,0,this.vacking,1, 'wash something', '#fffffff', 1, 'dont know what it was'),
    new Todosy(0,0,0,this.vacking,1, 'scrub the floor', '#000000', 1, 'With a toothbrush')];
  Idsorting = 0;
  nameSorting = 0;
  helphand = ['v','^'];
  sortSign = 'v';
  mailInput = '';
  passInput = '';
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  gg(s: User) {
    alert(`Hello ${s.Name}`);
  }
/*
  GenerateUser () {
    var NewUser = prompt('Enter your name', 'Petra Mandzukic');
    var NewMail = prompt('Enter your mail', 'bae@unitedempire.es2');
    var NewPassword = prompt ('Enter your Password');
    if (NewUser!= null && NewUser!= "") {
      this.tasks.push(new User(Math.floor(Math.random()*10), NewUser,NewMail, NewPassword));
    }
  }*/
/*
  pullFromNet() {
    let url = 'https://jsonplaceholder.typicode.com/users';
    let data = {"aa": 12, "gg": 'abrakadabra'};
    this.http.post<Todosy[]>(url, data).subscribe(result => {
      this.tasks = result;
    });
  }*/



  sortUserByID () {
    this.Idsorting++;
    if (this.Idsorting%2 === 1) {
      this.sortSign = this.helphand[0];
      this.tasks.sort(function (a,b) {return a.Id - b.Id});
    }    else {
      this.sortSign = this.helphand[1];
      this.tasks.sort(function (a,b) {return b.Id - a.Id});
    }
  }

  sortUserByName () {
    this.nameSorting++;

    if (this.nameSorting%2 === 1) {
      this.tasks.sort((a, b) => (a.Name.toUpperCase()>b.Name.toUpperCase())?1:-1);
    } else {
      this.tasks.sort((a, b) => (a.Name.toUpperCase()>b.Name.toUpperCase())?-1:1);
    }
  }
}
