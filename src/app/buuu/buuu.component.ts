import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Category} from "../_model/category";
import {Todosy} from "../_model/todosy";
import {log} from "util";
import {Task} from "protractor/built/taskScheduler";
import {GlobalsService} from "../globals.service";
import {forEach} from "@angular/router/src/utils/collection";
import {User} from "../_model/user";
import {interval, Observable, Subscription, timer} from "rxjs";

@Component({
  selector: 'app-buuu',
  templateUrl: './buuu.component.html',
  styleUrls: ['./buuu.component.less']
})
export class BuuuComponent implements OnInit {
  hera = 'HERA';
  categories : Category[];
  tasks = [];
  theUser = new User(-1, '', '', '');
  @Input()
  aa: string;
  email ='';
  pass ='';
  cattyId = -1;
  selectedId = -1;
  ggg = "#156856";
  abc: Subscription;

  constructor(private http: HttpClient, public g: GlobalsService) { }

  ngOnInit() {
    if (this.g.isLoggedIn){
      let data = {"Email": this.g.currMail, "Password": this.g.currPass};
      let categoryurl = 'http://alastor.overmc.net:34000/api/getcategories';
      this.http.post<Category[]>(categoryurl, data).subscribe(result => {
         this.categories = result;
      });
    }
    const source = interval(1000*60);
    let url = 'http://alastor.overmc.net:34000/api/updatetasks';
    let data = {};
    this.abc = source.subscribe(val => {
      let d = new Date();
      for (let f of this.tasks) {
        if (f.State == 1) {
          if (new Date(f.Deadline).getTime() - (1000*1800) <= d.getTime()){
            f.State = 2;
            data = {"Email": this.g.currMail, "Password": this.g.currPass,"Tasks": [f]};
            this.http.post(url, data).subscribe(result => {
              console.log(result);
            });
          }
        }
        if (f.State == 2) {
          if (new Date(f.Deadline).getTime() <= d.getTime()) {
            f.State = 4;
            data = {"Email": this.g.currMail, "Password": this.g.currPass,"Tasks": [f]};
            this.http.post(url, data).subscribe(result => {
              console.log(result);
            });
          }
        }
        if (f.State == 4) {
          if (new Date(f.Deadline).getTime() + (1000*3600*24) <= d.getTime()){
            f.State = 5;
            data = {"Email": this.g.currMail, "Password": this.g.currPass,"Tasks": [f]};
            this.http.post(url, data).subscribe(result => {
              console.log(result);
            });
          }
        }
        if (f.State == 3) {
          if (new Date(f.Deadline).getTime() + (1000*3600*24) <= d.getTime()) {
            f.State = 5;
            data = {"Email": this.g.currMail, "Password": this.g.currPass,"Tasks": [f]};
            this.http.post(url, data).subscribe(result => {
              console.log(result);
            });
          }
        }
      }
    });

  }

  login() {
    let loginurl = 'http://alastor.overmc.net:34000/api/login';
    let data = {"Email": this.email, "Password": this.pass};
    let categoryurl = 'http://alastor.overmc.net:34000/api/getcategories';
    let getuserurl = 'http://alastor.overmc.net:34000/api/getuser';
    let fock: object;
    this.tasks = [];
    this.http.post<object>(loginurl, data).subscribe(result => {
      fock = result;
      let fyck = new Boolean(fock);
      if (fyck) {
        this.g.currMail = this.email;
        this.g.currPass = this.pass;
        this.g.isLoggedIn = true;
        this.g.ggg = 'logged in!';
        this.http.post<Category[]>(categoryurl, data).subscribe(result => {
          this.categories = result;
        });

        this.http.post<User>(getuserurl, data).subscribe(result => {
          this.theUser = result;
          this.g.currUserid = this.theUser.Id;
          this.g.currName = this.theUser.Name;
          // console.log(this.g.currUserid);
          // console.log(this.g.currMail);
          // console.log(this.g.currPass);
          // console.log(this.g.currName); //Just a quick check to make sure im getting the right User
        });
      }
    });
  }

  showDescCat (s: Category) {
    return s.Description;
  }
  showDescrTask (s: Todosy) {
    return s.Description;
  }
  getTasks (x) {
    let gettasksurl = 'http://alastor.overmc.net:34000/api/gettask';
    let data = {"Email": this.g.currMail, "Password": this.g.currPass, "Category_Id": x};
    this.http.post<Todosy[]>(gettasksurl, data).subscribe(result => {
      this.tasks = result;
      this.tasks.sort((a,b) => (a.Position > b.Position) ? 1 : -1);
    });
  }
  getCategories () {
    let data = {"Email": this.g.currMail, "Password": this.g.currPass};
    let categoryurl = 'http://alastor.overmc.net:34000/api/getcategories';
    this.http.post<Category[]>(categoryurl,data).subscribe(result =>{
      this.categories = result;
    });
  }
  deleteCategory (x) {
    let deletedCategory = x;
    let url = 'http://alastor.overmc.net:34000/api/deletecategory';
    let data = {"Email": this.g.currMail, "Password": this.g.currPass, "Category": deletedCategory};

    this.http.post(url, data).subscribe(result => {
      console.log(result);
      this.getCategories();

    });
  }
  showTask (x) {
    this.selectedId = x;
  }
  completeTask(x) {
    x.State = 3;
    let url = 'http://alastor.overmc.net:34000/api/updatetasks';
    let data = {"Email": this.g.currMail, "Password": this.g.currPass, "Tasks": [x,x]};
    // console.log(data.Tasks[0].Name);
    this.http.post(url, data).subscribe(result => {
      console.log(result);
    });
  }
  deleteTask (x){
    let url = 'http://alastor.overmc.net:34000/api/deletetask';
    let data = {"Email": this.g.currMail, "Password": this.g.currPass, "Task": x};
    this.http.post(url, data).subscribe(result => {
      console.log(result);
      this.getTasks(x.Category_Id);
    });
  }

  logout () {
    this.g.currName = '';
    this.g.currMail = '';
    this.g.currPass = '';
    this.g.currUserid = -1;
    this.g.isLoggedIn = false;
    this.tasks = [];
    this.categories = [];
  }
  upTask (id) {
    let isFirst = true;
    let checkThis: Todosy;
    let swapThis: Todosy;
    let swapper = -1;
    let url = 'http://alastor.overmc.net:34000/api/updatetasks';
    for (let r = 0; r < this.tasks.length; r++){
      checkThis = this.tasks[r];
      if(checkThis.State == 1 || checkThis.State == 2 || checkThis.State == 4) {
        if (checkThis.Id == id) {
          if (isFirst) {
            return;
          }
          else {
            swapper = checkThis.Position;
            checkThis.Position = swapThis.Position;
            swapThis.Position = swapper;
            let data = {"Email": this.g.currMail, "Password": this.g.currPass,"Tasks": [checkThis, swapThis]};
            this.http.post(url, data).subscribe(result => {
              this.getTasks(this.cattyId);
            });
          }
        }
        isFirst = false;
        swapThis = checkThis;
      }
    }
  }
  downTask (id) {
    let isFirst = true;
    let checkThis: Todosy;
    let swapThis: Todosy;
    let swapper = -1;
    let url = 'http://alastor.overmc.net:34000/api/updatetasks';
    for (let r = this.tasks.length - 1; r >= 0; r--){
      checkThis = this.tasks[r];
      if(checkThis.State == 1 || checkThis.State == 2 || checkThis.State == 4) {
        if (checkThis.Id == id) {
          if (isFirst) {
            return;
          }
          else {
            swapper = checkThis.Position;
            checkThis.Position = swapThis.Position;
            swapThis.Position = swapper;
            let data = {"Email": this.g.currMail, "Password": this.g.currPass,"Tasks": [checkThis, swapThis]};
            this.http.post(url, data).subscribe(result => {
              this.getTasks(this.cattyId);
            });
          }
        }
        isFirst = false;
        swapThis = checkThis;
      }
    }
  }
}
