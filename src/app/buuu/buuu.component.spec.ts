import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuuuComponent } from './buuu.component';

describe('BuuuComponent', () => {
  let component: BuuuComponent;
  let fixture: ComponentFixture<BuuuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuuuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuuuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
