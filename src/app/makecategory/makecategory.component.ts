import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {GlobalsService} from "../globals.service";
import {removeSummaryDuplicates} from "@angular/compiler";
import {Category} from "../_model/category";
import {BuuuComponent} from "../buuu/buuu.component";

@Component({
  selector: 'app-makecategory',
  templateUrl: './makecategory.component.html',
  styleUrls: ['./makecategory.component.less']
})
export class MakecategoryComponent implements OnInit {

  constructor(private http: HttpClient, public g: GlobalsService, public b: BuuuComponent) { }
  User_Id = this.g.currUserid;
  Id = 0;
  Name = '';
  Description = '';
  Color = '';
  Position = -1;
  categories: Category[];

  ngOnInit() {
  }

  slapCategory(nameCat, descCat, colCat) {
    let url = 'http://alastor.overmc.net:34000/api/addcategory';
    let object = {"Id": this.Id, "User_Id": this.User_Id, "Name": this.Name, "Color": this.Color,
      "Position": this.Position, "Description": this.Description};
    let data = {"Email": this.g.currMail, "Password": this.g.currPass, "Category": object};
    this.http.post<boolean>(url, data).subscribe(result => {
      if (result) {
        alert('Created new Category');
        this.b.getCategories();
      }
    });
  }
}
