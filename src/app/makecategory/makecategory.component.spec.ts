import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakecategoryComponent } from './makecategory.component';

describe('MakecategoryComponent', () => {
  let component: MakecategoryComponent;
  let fixture: ComponentFixture<MakecategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakecategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakecategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
