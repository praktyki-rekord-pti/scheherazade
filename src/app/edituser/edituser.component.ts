import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {GlobalsService} from "../globals.service";

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.less']
})
export class EdituserComponent implements OnInit {

  constructor(private http: HttpClient, public g: GlobalsService) { }

  newName = this.g.currName;
  passCheck = '';
  newPass = '';

  updateUser () {
    if (this.passCheck == this.g.currPass) {
      let url = 'http://alastor.overmc.net:34000/api/updateuser';
      if (this.newName == '') {
        alert('Pick a name, you uncultured swine');
        return;
      }
      if (this.newPass == '') {
        this.newPass = this.g.currPass;
      }
      let newUser = {"Id": this.g.currUserid, "Name": this.newName, "Email": this.g.currMail, "Password": this.newPass};
      let data = {"Email": this.g.currMail, "Password": this.g.currPass, "User": newUser};
      this.http.post(url, data).subscribe(result =>{
        console.log(result);
        this.g.currName = this.newName;
        this.g.currPass = this.newPass;
      });
    }
    else {
      alert('Wrong Password');
    }
  }


  ngOnInit() {
  }

}
