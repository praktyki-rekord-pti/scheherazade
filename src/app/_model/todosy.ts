export class Todosy {
  Id: number;
  Category_Id: number;
  // User_Id: number;
  Deadline: Date;
  State: number;
  Name: string;
  Color: string;
  Position: number;
  Description: string;

  constructor(taskId: number, catId: number, userId: number, DeadLine: Date, state: number, name: string, color: string,
              position: number, descr: string) {
    this.Id = taskId;
    this.Category_Id = catId;
    // this.User_Id = userId;
    this.Deadline = DeadLine;
    this.State = state;
    this.Name = name;
    this.Color = color;
    this.Position = position;
    this.Description = descr;
  }
}
