export class User {
  Id: number;
  Name: string;
  Email: string;
  Password: string;

  constructor(userId: number, name: string, email: string, password: string) {
    this.Id = userId;
    this.Name = name;
    this.Email = email;
    this.Password = password;
  }
}
