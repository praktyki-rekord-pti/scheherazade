export class Category {
  Id: number;
  User_Id: number;
  Name: string;
  Color: string;
  Position: number;
  Description: string;

  constructor (id: number, userId: number, name: string, color: string, position: number, descr: string){
    this.Id = id;
    this.User_Id = userId;
    this.Name = name;
    this.Color = color;
    this.Position = position;
    this.Description = descr;
  }

}
