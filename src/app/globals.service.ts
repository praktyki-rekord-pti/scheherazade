import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalsService {
  url = 'http://10.10.0.55:8082';
  token = '123';
  ggg = 'abc';
  currMail = '';
  currPass = '';
  currUserid = -1;
  currName: string;
  isLoggedIn = false;
  constructor() { }

  q(path):string {
    return this.url + path + '&token=' + this.token;
  }
}
