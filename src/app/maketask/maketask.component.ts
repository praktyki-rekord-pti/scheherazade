import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {GlobalsService} from "../globals.service";
import {User} from "../_model/user";
import {Todosy} from "../_model/todosy";
import {Category} from "../_model/category";
import {Time} from "@angular/common";
import {stringify} from "querystring";

@Component({
  selector: 'app-maketask',
  templateUrl: './maketask.component.html',
  styleUrls: ['./maketask.component.less']
})
export class MaketaskComponent implements OnInit {

  hereUser = new User(-1, '', '', '');
  categories: Category[];
  Category_Id = 0;
  User_Id = 0;
  Timewasted: string;
  Dategiven: string;
  Category_name;
  Deadline = new Date();
  Name = '';
  Color = '';
  Description = '';
  dateShite = new Date();
  ggggg: any;


  constructor(private http: HttpClient, public g: GlobalsService) { }

  ngOnInit() {
    if (this.g.currMail != ''){
      let urlUser = 'http://alastor.overmc.net:34000/api/getuser';
      let data = {"Email": this.g.currMail, "Password": this.g.currPass};

      this.http.post<User>(urlUser, data).subscribe(result => {
        this.hereUser = result;
        this.User_Id = this.hereUser.Id;
        let dataCategory = {"Email": this.g.currMail, "Password": this.g.currPass};
        let urlCategory = 'http://alastor.overmc.net:34000/api/getcategories';
        this.http.post<Category[]>(urlCategory, dataCategory).subscribe(result => {
          this.categories = result;
        });
      });
    }
  }
  setCategory(id,name) {
    this.Category_Id = id;
    this.Category_name = name;
  }
  slapTask (nameTask, describeTask, colorTask) {
    if (this.Category_Id != 0) {
      this.updateDate();
      let url = 'http://alastor.overmc.net:34000/api/addtask';
      let object = {"Id": 0, "Category_Id": this.Category_Id, "Deadline": this.Deadline,
        "State": 1, "Name": nameTask, "Color": colorTask, "Position": -1, "Description": describeTask};

      let data = {"Email": this.g.currMail, "Password": this.g.currPass, "Task": object};
      console.log(data);
      this.http.post<boolean>(url, data).subscribe(result => {
        let boo = Boolean(result);
        if (result) {
          alert('Task created succesfully, with category ' + this.Category_Id + ' for user ' + this.g.currMail);
        }
      });
    }
    else alert('Select a Category first');
  }
  dateDiff () {
    console.log(this.Deadline.getDay() - this.dateShite.getDay());
  }
  showColor() {
    console.log(this.ggggg);
  }

  updateDate() {
    let kashaar = this.Dategiven + 'T' + this.Timewasted + '.000+00:00';
    this.Deadline = new Date(kashaar);
  }

}
