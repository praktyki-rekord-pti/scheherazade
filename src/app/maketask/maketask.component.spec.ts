import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaketaskComponent } from './maketask.component';

describe('MaketaskComponent', () => {
  let component: MaketaskComponent;
  let fixture: ComponentFixture<MaketaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaketaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaketaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
