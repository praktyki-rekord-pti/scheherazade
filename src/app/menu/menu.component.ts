import { Component, OnInit } from '@angular/core';
import {GlobalsService} from "../globals.service";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.less']
})
export class MenuComponent implements OnInit {

  constructor(public g: GlobalsService) { }

  ngOnInit() {
  }

}
