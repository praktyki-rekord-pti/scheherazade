import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {GlobalsService} from "../globals.service";
import {Category} from "../_model/category";
import {Task} from "protractor/built/taskScheduler"; //5
import {forEach} from "@angular/router/src/utils/collection";
import {Todosy} from "../_model/todosy";

@Component({
  selector: 'app-archive',      //10
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.less']
})
export class ArchiveComponent implements OnInit {
//15
  categories: Category[];
  tasks: Todosy[];
  allTheTasks = [new Todosy(0,3,1,new Date(),1,'Boo','#222222',-1, 'oh shit i got scared')];
  constructor(private http: HttpClient, public g: GlobalsService) { }
//20

  ngOnInit() {
    this.allTheTasks =  [];
    console.log(typeof this.allTheTasks);
    let dataCategory = {"Email": this.g.currMail, "Password": this.g.currPass};     //25
    let urlCategory = 'http://alastor.overmc.net:34000/api/getcategories';
    this.http.post<Category[]>(urlCategory, dataCategory).subscribe(result => {
      this.categories = result;
      for (let catcat = 0; catcat < this.categories.length; catcat++){
        this.getTasks(this.categories[catcat].Id);



      }
    });
  }
  getTasks (x) {
    let gettasksurl = 'http://alastor.overmc.net:34000/api/gettask';
    let data = {"Email": this.g.currMail, "Password": this.g.currPass, "Category_Id": x};
    this.http.post<Todosy[]>(gettasksurl, data).subscribe(result => {
      this.tasks = result;
      for (let tododo = 0; tododo < this.tasks.length; tododo++){
        this.allTheTasks.push(this.tasks[tododo]);
      }
    });
  }

}
