import { Injectable } from '@angular/core';
import {GlobalsService} from "./globals.service";
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class LoginGuardService implements CanActivate {

  constructor(public g: GlobalsService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : boolean  {
    return this.g.isLoggedIn;
  }


}
