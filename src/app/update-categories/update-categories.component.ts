import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {GlobalsService} from "../globals.service";
import {Category} from "../_model/category";
import {removeSummaryDuplicates} from "@angular/compiler";

@Component({
  selector: 'app-update-categories',
  templateUrl: './update-categories.component.html',
  styleUrls: ['./update-categories.component.less']
})
export class UpdateCategoriesComponent implements OnInit {

  constructor(private http: HttpClient, public g: GlobalsService) { }
  pickCat = false;
  selectCat = new Category(-1,-1,'','',-1,'');
  categories: Category[];

  ngOnInit() {
    this.getCategories();
  }
  getCategories () {
    let data = {"Email": this.g.currMail, "Password": this.g.currPass};
    let categoryurl = 'http://alastor.overmc.net:34000/api/getcategories';
    this.http.post<Category[]>(categoryurl,data).subscribe(result =>{
      this.categories = result;
    });
  }

  pickedCat (x) {
    this.pickCat = true;
    this.selectCat = x;
  }
  updateCategories() {
    for (let catLooker = 0; catLooker < this.categories.length; catLooker++) {
      if (this.categories[catLooker].Id == this.selectCat.Id){
        this.categories[catLooker] = this.selectCat;
      }
    }
    let url = 'http://alastor.overmc.net:34000/api/updatecategories';
    let data = {"Email": this.g.currMail, "Password": this.g.currPass, "Categories": this.categories};
    this.http.post(url,data).subscribe(result =>{
      console.log(result);
      this.getCategories();
    });
  }
}
