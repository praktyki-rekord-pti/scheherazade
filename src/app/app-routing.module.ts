import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BuuuComponent} from "./buuu/buuu.component";
import {MaketaskComponent} from "./maketask/maketask.component";
import {EdituserComponent} from "./edituser/edituser.component";
import {LoginGuardService} from "./login-guard.service";
import {UpdateCategoriesComponent} from "./update-categories/update-categories.component";
import {ArchiveComponent} from "./archive/archive.component";

const routes: Routes = [
  { path: '', redirectTo: 'buuu', pathMatch: 'full'},
  { path: 'buuu', component: BuuuComponent},
  { path: 'maketask', component: MaketaskComponent, canActivate: [LoginGuardService]},
  { path: 'edituser', component: EdituserComponent, canActivate: [LoginGuardService]},
  { path: 'update-categories', component: UpdateCategoriesComponent, canActivate: [LoginGuardService]},
  { path: 'archive', component: ArchiveComponent, canActivate: [LoginGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
